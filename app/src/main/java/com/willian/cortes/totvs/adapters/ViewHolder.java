package com.willian.cortes.totvs.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.willian.cortes.totvs.R;


public class ViewHolder extends RecyclerView.ViewHolder {
    public TextView txNome;
    public TextView txCpf;
    public TextView txId;

    public ViewHolder(View itemView) {
        super(itemView);

        this.txNome = itemView.findViewById(R.id.item_nome);
        this.txCpf = itemView.findViewById(R.id.item_cpf);
        this.txId = itemView.findViewById(R.id.item_id);
    }
}
