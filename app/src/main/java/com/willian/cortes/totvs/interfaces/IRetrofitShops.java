package com.willian.cortes.totvs.interfaces;

import com.willian.cortes.totvs.constants.Constants;
import com.willian.cortes.totvs.models.Shop;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface IRetrofitShops {

    @Headers("Authorization: " + Constants.KEY)
    @GET("cnpj/")
    Call<List<Shop>> getShops();

    public static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://integration.dev.bemacashdev.click/prophet/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();
}
