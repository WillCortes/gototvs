package com.willian.cortes.totvs.interfaces;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.willian.cortes.totvs.models.Shop;

import java.util.List;

@Dao
public interface ShopDAO {
    @Insert
    void insertShop(Shop shop);

    @Query("SELECT * FROM shop")
    List<Shop> findAllShops();

    @Query("DELETE  FROM shop")
    void deleteAllShops();

    @Update
    void updateShop(Shop shop);

    @Delete
    void deleteShop(Shop shop);
}
