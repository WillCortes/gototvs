package com.willian.cortes.totvs.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;
import com.willian.cortes.totvs.constants.Constants;

import java.io.Serializable;

@Entity(tableName = Constants.NAME_TABLE_SHOP)
public class Shop implements Serializable {
    @PrimaryKey
    @ColumnInfo(name = "shopId")
    @SerializedName("shopId")
    private int shopId;

    @ColumnInfo(name = "cnpj")
    @SerializedName("cnpj")
    private String cnpj;

    @ColumnInfo(name = "cpf")
    @SerializedName("cpf")
    private String cpf;

    @ColumnInfo(name = "shopType")
    @SerializedName("shopType")
    private String shopType;

    @ColumnInfo(name = "nome")
    @SerializedName("nome")
    private String nome;

    @ColumnInfo(name = "email")
    @SerializedName("email")
    private String email;

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getShopType() {
        return shopType;
    }

    public void setShopType(String shopType) {
        this.shopType = shopType;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
