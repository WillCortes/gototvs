package com.willian.cortes.totvs.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.willian.cortes.totvs.constants.Constants;
import com.willian.cortes.totvs.interfaces.ShopDAO;
import com.willian.cortes.totvs.models.Shop;

@Database(entities = {Shop.class}, version = 1)
public abstract class AppDb extends RoomDatabase {

    private static AppDb INSTANCE;
    public abstract ShopDAO shopDAO();

    public static AppDb getAppDb(Context context){
        if(INSTANCE == null){
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDb.class, Constants.NAME_DATABASE)
                    .allowMainThreadQueries()
                    .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance(){
        INSTANCE = null;
    }

}
