package com.willian.cortes.totvs.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.willian.cortes.totvs.R;
import com.willian.cortes.totvs.models.Shop;

import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<ViewHolder> {
    private List<Shop> list;

    public RecyclerAdapter(List<Shop> list) {
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Shop shop = list.get(position);
        holder.txNome.setText(shop.getNome());
        holder.txCpf.setText("CPF: " + shop.getCpf());
        holder.txId.setText("ID: " + String.valueOf(shop.getShopId()) + "\n" + shop.getEmail());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
