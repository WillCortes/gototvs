package com.willian.cortes.totvs.activitys;

import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.willian.cortes.totvs.R;
import com.willian.cortes.totvs.adapters.RecyclerAdapter;
import com.willian.cortes.totvs.database.AppDb;
import com.willian.cortes.totvs.interfaces.IRetrofitShops;
import com.willian.cortes.totvs.models.Shop;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private ActionBar actionBar = null;
    private Toolbar toolbar;

    private RecyclerView recyclerView;
    private RecyclerAdapter adapter;
    private ProgressBar progressBar;
    private List<Shop> shops;
    private SaveDataBaseTask saveDataBaseTask;
    private ReadDataBaseTask readDataBaseTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        configView();
        callRetrofit();

    }

    private void callRetrofit() {
        AppDb.getAppDb(getApplicationContext()).shopDAO().deleteAllShops();

        IRetrofitShops retrofitShops = IRetrofitShops.retrofit.create(IRetrofitShops.class);
        final Call<List<Shop>> call = retrofitShops.getShops();
        call.enqueue(new Callback<List<Shop>>() {
            private Shop shop;

            @Override
            public void onResponse(Call<List<Shop>> call, Response<List<Shop>> response) {
                int code = response.code();
                if (code == 200) {
                    List<Shop> lista = response.body();
                    for (Shop shop : lista) {
                        this.shop = new Shop();
                        this.shop.setShopId(shop.getShopId());
                        this.shop.setCnpj(shop.getCnpj());
                        this.shop.setCpf(shop.getCpf());
                        this.shop.setShopType(shop.getShopType());
                        this.shop.setNome(shop.getNome());
                        this.shop.setEmail(shop.getEmail());

                        saveDataBaseTask = new SaveDataBaseTask();
                        saveDataBaseTask.execute(this.shop);
                    }
                } else {
                    progressBar.setVisibility(View.GONE);
                    msgFailure();
                }
            }

            @Override
            public void onFailure(Call<List<Shop>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                msgFailure();
            }

        });
    }

    private void configView() {
        shops = new ArrayList<>();

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(getResources().getString(R.string.app_name));

        progressBar = findViewById(R.id.circularBar);
        recyclerView = findViewById(R.id.MainActivity_recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class SaveDataBaseTask extends AsyncTask<Shop, Void, Void> {

        @Override
        protected Void doInBackground(Shop... shops) {
            AppDb.getAppDb(getApplicationContext()).shopDAO().insertShop(shops[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            readDataBaseTask = new ReadDataBaseTask();
            readDataBaseTask.execute();
        }
    }

    private class ReadDataBaseTask extends AsyncTask<Void, Void, List<Shop>> {

        @Override
        protected List<Shop> doInBackground(Void... voids) {
            shops = AppDb.getAppDb(getApplicationContext()).shopDAO().findAllShops();
            return shops;
        }

        @Override
        protected void onPostExecute(List<Shop> shops) {
            progressBar.setVisibility(View.GONE);
            adapter = new RecyclerAdapter(shops);
            recyclerView.setAdapter(adapter);
        }
    }

    private void msgFailure() {
        View parentLayout = findViewById(android.R.id.content);
        Snackbar.make(parentLayout, getResources().getString(R.string.erro_conexao), Snackbar.LENGTH_LONG).show();
    }
}
