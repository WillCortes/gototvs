package com.willian.cortes.totvs;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({KeyTest.class, ExampleUnitTest.class})
public class AllTests {
}
