package com.willian.cortes.totvs;

import com.willian.cortes.totvs.constants.Constants;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class KeyTest {

    private Constants constants;

    @Before
    public void beforeTest() {
        constants = new Constants();
    }

    @Test
    public void validateHashAuthentication() {
        Assert.assertEquals(constants.KEY.length(), 36);
    }
}
